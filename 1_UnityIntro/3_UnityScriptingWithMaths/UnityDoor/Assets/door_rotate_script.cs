﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class door_rotate_script : MonoBehaviour
{
    public GameObject door;
    GameObject myPlayer;
    public float targetRot = 90f;
    bool doorOpen;
    public float speed = 0.0000005f;
    Vector3 closedRot;
    public mouse_listener mouse;
    public Image cursorImage;
    bool inTrigger;
    bool clickHappened;
    // Use this for initialization
    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        closedRot = door.transform.localRotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if(inTrigger == true)
        {
            if(mouse.mouseCursorOn == true)
            {
                if(cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;
                }
                
            }
            else
            {
                cursorImage.enabled = false;
            }

            if (inTrigger == true && mouse.mouseClicked == true && clickHappened == false)
            {
                clickHappened = true;
                DoorInteract();
            }
            else if(mouse.mouseClicked == false)
            {
                clickHappened = false;
            }
        }
    }
    //Door Open
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }
    //Door Close
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = false;
            cursorImage.enabled = false;
        }
    }

    void DoorInteract()
    {
        Vector3 finishedRot;
        if (doorOpen == false)
        {
            Vector3 playerDir = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(playerDir, transform.forward);
            Debug.Log(dot);
            doorOpen = true;
            if(dot > 0f)
            {
                finishedRot = new Vector3(closedRot.x, closedRot.y + targetRot, closedRot.z);
            }
            else
            {
                finishedRot = new Vector3(closedRot.x, closedRot.y - targetRot, closedRot.z);
            }
        }
        else
        {
            finishedRot = closedRot;
            doorOpen = false;
        }
        StopCoroutine("DoorMotion");
        StartCoroutine("DoorMotion",finishedRot);
        
    }
    IEnumerator DoorMotion(Vector3 target)
    {
        while (Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) > 0.02f)
        {
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), speed * Time.deltaTime);
            yield return null;
        }
        door.transform.localRotation = Quaternion.Euler(target);
        yield return null;
    }
}