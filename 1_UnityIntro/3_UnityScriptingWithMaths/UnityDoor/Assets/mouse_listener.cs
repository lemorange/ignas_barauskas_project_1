﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouse_listener : MonoBehaviour
{
    public bool mouseCursorOn;
    public bool mouseClicked;

    void OnMouseOver()
    {
        if(mouseCursorOn == false)
        {
            mouseCursorOn = true;
        }
    }
    void OnMouseExit()
    {
        mouseCursorOn = false;
    }
    void OnMouseDown()
    {
        mouseClicked = true;
    }
    void OnMouseUp()
    {
        mouseClicked = false;
    }
}
