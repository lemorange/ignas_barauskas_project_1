﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sliding_door_script : MonoBehaviour {

    public Transform door1Tran;
    public Transform door2Tran;
    public float moveAmount = 1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //Door Open
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("player in trigger");
            //door1Tran.localPosition = new Vector3(moveAmount, 0, 0);
            //door2Tran.localPosition = new Vector3(-moveAmount, 0, 0);
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", moveAmount);
        }
    }
    //Door Close
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            //door1Tran.localPosition = new Vector3(0, 0, 0);
            //door2Tran.localPosition = new Vector3(0, 0, 0);
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
            Debug.Log("player left trigger");
        }
    }
    IEnumerator DoorMove(float target)
    {
        float xPos = door1Tran.localPosition.x;
        Debug.Log("Started");
        while (xPos < (target - 0.02f) || xPos > (target + 0.02f))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, 5 * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Finished");
        yield return null;
    }
}
