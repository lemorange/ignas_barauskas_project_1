﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lightswitch : MonoBehaviour
{
    public Trigger_Listener trigger;
    public Image cursorImage;
    public Light spotLight;
    public AudioSource sound;
    Animation anim;

    // Use this for initialization
    void Start()
    {
        sound = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
        cursorImage.enabled = false;
        spotLight.intensity = 0f;
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
            Debug.Log("Moused over switch");
        }
    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            sound.Play();
            anim.Stop();
            anim.Play();
            if(spotLight.intensity > 0f)
            {
                spotLight.intensity = 0f;
            }
            else
            {
                spotLight.intensity = 3f;
            }
        }
    }
}

