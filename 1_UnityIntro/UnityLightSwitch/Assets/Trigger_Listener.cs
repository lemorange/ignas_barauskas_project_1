﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//These are name spaces and locations of code.


    //This is a class that inherits from MonoBehaviour
public class Trigger_Listener : MonoBehaviour
{
    public bool playerEntered = false;

	// Use this for initialization
	void Start ()
    {
        Debug.Log("Start Was Called");

	}
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log("Update Was Called");
	}
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }

    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }

    }


}
